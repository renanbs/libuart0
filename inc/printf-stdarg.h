#ifndef PRINTF_STDARG_H_
#define PRINTF_STDARG_H_

int sprintf(char *out, const char *format, ...);
int snprintf( char *buf, unsigned int count, const char *format, ... );
int printf(const char *format, ...);

#endif /* PRINTF_STDARG_H_ */
